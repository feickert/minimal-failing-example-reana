#!/bin/bash

# Allow pip to only run in a virtual environment; exit with an error otherwise.
# https://pip.pypa.io/en/stable/cli/pip/#cmdoption-require-virtualenv
export PIP_REQUIRE_VIRTUALENV=true

python -m pip install --upgrade pip setuptools wheel
python -m pip install \
    --no-deps \
    --require-hashes \
    --use-pep517 \
    --requirement requirements.lock

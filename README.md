# minimal-failing-example-reana

This repo demonstrates that there is a problem with REANA runtimes as the `busybox` images.

## Setup

1. Create a Python virtual environment, activate it, and then install the environment specified in the `requirements.lock` lock file.

```console
$ python -m venv venv
$ . venv/bin/activate
$ bash install.sh
```

2. Create a `.env.sh` file and define the variables `REANA_SERVER_URL` and `REANA_ACCESS_TOKEN` inside of it.

```console
$ touch .env.sh
$ cat .env.sh
#!/bin/bash

# Set up environment variables to access the REANA instance at CERN
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN="XXXXXXXXXXXXXXXXXXX"
```

## Demo

The only difference between `reana-fail.yaml` and `reana-pass.yaml` is the image used in the `environment` variable.

```diff
$ diff -u reana-fail.yaml reana-pass.yaml
--- reana-fail.yaml	2022-12-08 23:17:22.949554700 -0600
+++ reana-pass.yaml	2022-12-08 23:17:12.981607618 -0600
@@ -10,7 +10,7 @@
   specification:
     steps:
       - name: example
-        environment: 'busybox:1.35.0'
+        environment: 'debian:bullseye-slim'
         kubernetes_memory_limit: '256Mi'
         commands:
         - mkdir -p -v results
```

Both REANA spec files pass `reana-client validate` yet the submitted job from `submit_fail.sh` will fail and the job from `submit_pass.sh` will succeed. So it seems that there is some problem with REANA and `busybox`.

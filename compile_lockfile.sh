#!/bin/bash

# Allow pip to only run in a virtual environment; exit with an error otherwise.
# https://pip.pypa.io/en/stable/cli/pip/#cmdoption-require-virtualenv
export PIP_REQUIRE_VIRTUALENV=true

python -m venv /usr/local/venv
. /usr/local/venv/bin/activate
python -m pip install --upgrade pip setuptools wheel
python -m pip install --upgrade pip-tools

pip-compile \
    --generate-hashes \
    --output-file requirements.lock \
    requirements.txt

# Make it easier to convert file to user control from root in lock.sh
mv requirements.lock _requirements.lock

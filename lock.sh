#!/bin/bash

builder_image="python:3.10"
docker pull "${builder_image}"

docker run --rm -ti \
    -v $PWD:/work \
    -w /work \
    "${builder_image}" /bin/bash compile_lockfile.sh

cp _requirements.lock requirements.lock
rm _requirements.lock

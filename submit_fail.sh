#!/bin/bash

# Set up environment variables to access the REANA instance at CERN
# export REANA_SERVER_URL=https://reana.cern.ch/
# export REANA_ACCESS_TOKEN="XXXXXXXXXXXXXXXXXXX"
. ./.env.sh

tag=$(coolname 2)
workflow_name="minimal-failing-example-${tag}"

# Validate REANA spec file
reana-client validate \
    --file reana-fail.yaml \
    --environments \
    --pull

# Create and submit REANA workflow to run
reana-client create \
    --file reana-fail.yaml \
    --workflow "${workflow_name}"
export REANA_WORKON="${workflow_name}"
reana-client upload
reana-client start
reana-client status

echo ""
echo "# Check https://reana.cern.ch/"

# sleep 120

# reana-client status
# reana-client logs
# reana-client ls
# reana-client download results/out.txt
